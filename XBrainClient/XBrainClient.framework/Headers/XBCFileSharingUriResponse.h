//
//  XBCFileSharingUriResponse.h
//  XBrainClient
//
//  Created by xBrainsoft on 24/06/15.
//  Copyright (c) 2015 Nicolas Mauri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XBCCustomMessagePrototype.h"

/**
 * Class defining a message that is received and gives the URI to use for "file sharing with agent" features.
 */
@interface XBCFileSharingUriResponse : XBCCustomMessagePrototype

/**
 * The given URI
 */
@property (nonatomic, readonly, nullable) NSString *uri;

@end
