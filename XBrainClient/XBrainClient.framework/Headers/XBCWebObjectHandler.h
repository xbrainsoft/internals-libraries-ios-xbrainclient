//
//  WebObjectHandler.h
//  XBrainClient
//
//  Created by Nicolas Mauri on 1/11/13.
//  Copyright (c) 2013 XBrain. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "XBCWebObject.h"
#import "XBCWebObjectViewController.h"

/**
 * XBCWebObjectHandler
 */
@protocol XBCWebObjectHandler <NSObject>

/**
 * handleWebObject
 *
 *  @param webObject      a <i>XBCWebObject</i>
 *  @param viewController a <i>XBCWebObjectViewController</i>
 */
- (void)handleWebObject:(XBCWebObject *)webObject
         withController:(XBCWebObjectViewController *)viewController;

@end
