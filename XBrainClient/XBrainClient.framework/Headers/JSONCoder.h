//
//  JSONCoder.h
//  XBrainClient
//
//  Created by Nicolas Mauri on 06/12/12.
//  Copyright (c) 2012 XBrain. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 * <i>NSCoding</i> "sub-protocol".
 * @discussion <i>XBCCustomMessagePrototype</i> subclasses need to implement this protocol to allow serialization / deserialization
 */
@protocol JSONCoding <NSCoding>

@end

/**
 * Class used for serializing custom message prototypes
 */
@interface JSONCoder : NSCoder

/**
 * Serialize rootObject in JSON
 * @param rootObject rootObject
 */
+ (nullable NSData *)archivedJSONDataWithRootObject:(null_unspecified id)rootObject;

@end

/**
 * Class used for de-serializing custom message prototypes
 */
@interface JSONDecoder : NSCoder

/**
 * initForReadingWithJSON
 *
 * @param value value
 */
- (nonnull instancetype)initForReadingWithJSON:(nonnull NSDictionary *)value;

/**
 * deserialize json
 * @param jsonObject jsonObject
 */
+ (nullable id)unarchiveObjectWithJSONObject:(nonnull id)jsonObject;

/**
 * deserialize the value specified by "key" into a array of the given class
 *
 *  @param classObject a class
 *  @param key         a key
 *
 *  @return an array
 */
- (nullable NSArray *)decodeArrayOfClass:(nonnull Class)classObject forKey:(nonnull NSString *)key;

/**
 * decodeObjectForKey
 *
 *  @param key a key
 *  @param classObject a class
 */
- (nullable id)decodeObjectForKey:(nonnull NSString *)key usingClass:(nullable Class)classObject;

@end
