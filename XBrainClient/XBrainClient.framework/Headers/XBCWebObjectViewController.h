//
//  WebObjectViewController.h
//  XBrainClient
//
//  Created by Nicolas Mauri on 31/07/12.
//  Copyright (c) 2012 XBrain. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XBCWebObject;

/**
 * Protocol defining a webobject delegate
 */
@protocol XBCWebObjectViewControllerDelegate <NSObject>

/**
 * Webobject has been loaded
 */
- (void)webObjectLoaded;

/**
 * Webobject's size has changed
 *
 *  @param webObject a <i>XBCWebObject</i>
 *  @param view a <i>UIView</i>
 */
- (void)webObject:(XBCWebObject *)webObject sizeChangedForView:(UIView *)view;

@end

/**
 * XBCWebObjectViewController
 */
@interface XBCWebObjectViewController : UIViewController <UIWebViewDelegate>

/**
 * webview
 */
@property (strong, nonatomic) UIWebView *webview;

/**
 * delegate
 */
@property (weak, nonatomic) id<XBCWebObjectViewControllerDelegate> delegate;

/**
 * Load the given webobject
 *
 *  @param webobject a <i>XBCWebObject</i>
 */
- (void)loadWebObject:(XBCWebObject *)webobject;

@end
