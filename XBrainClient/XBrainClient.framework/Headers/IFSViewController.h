//
//  IFSViewController.h
//  XBrainClient
//
//  Created by Nicolas Mauri on 30/01/12.
//  Copyright (c) 2012 XBrain. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * Protocol defining a delegate for a <i>IFSViewController</i>
 */
@protocol IFSViewControllerDelegate <NSObject>

- (void)ifsCreateIdentityDidSucceedWithClientID:(null_unspecified NSString *)clientID andSharedKey:(null_unspecified NSString *)sharedKey;
- (void)ifsCreateIdentityDidFailedWithError:(null_unspecified NSError *)error;
- (void)ifsDidComplete;

@end

/**
 *  <i>IFSViewController</i>
 */
@interface IFSViewController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic, null_unspecified) UIWebView *webView;
@property (strong, nonatomic, null_unspecified) UIToolbar *toolbar;
@property (strong, nonatomic, null_unspecified) UIBarButtonItem *backButton;
@property (strong, nonatomic, null_unspecified) UIBarButtonItem *closeButton;
@property (strong, nonatomic, null_unspecified) UIActivityIndicatorView *activityIndicator;

NS_ASSUME_NONNULL_BEGIN

/**
 * <i>IFSViewControllerDelegate</i>
 */
@property (strong, nonatomic, nullable) id<IFSViewControllerDelegate> delegate;

/**
 *  Open IFS registration
 *
 *  @param url           url
 *  @param applicationId applicationId
 *  @param deviceID      deviceID
 *  @param languageCode  languageCode
 */
- (void)openIFSRegistrationWithURL:(NSURL *)url
                     applicationId:(NSString *)applicationId
                  deviceIdentifier:(NSString *)deviceID
                   andLanguageCode:(NSString *)languageCode;

/**
 *  Open IFS home
 *
 *  @param url url
 */
- (void)openIFSHomeWithURL:(NSURL *)url;

NS_ASSUME_NONNULL_END

@end
