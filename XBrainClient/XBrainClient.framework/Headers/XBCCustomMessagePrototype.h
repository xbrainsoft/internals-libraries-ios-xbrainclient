//
//  XBCCustomMessagePrototype.h
//  XBrainClient
//
//  Created by Nicolas Mauri on 07/12/12.
//  Copyright (c) 2012 XBrain. All rights reserved.
//

#import "XBCCustomMessage.h"
#import "JSONCoder.h"

/**
 * Base class for custom messages' prototypes
 * @discussion Base class for custom message prototype. A prototype must override the <i>+(NSString *)key</i> method and
 * implement <i>JSONCoding</i> protocol
 */
@interface XBCCustomMessagePrototype : XBCCustomMessage <JSONCoding>
NS_ASSUME_NONNULL_BEGIN

/**
 * The key defining this kind of message
 * @discussion The key corresponding to this class. TO OVERRIDE.
 * @warning TO OVERRIDE.
 */
+ (NSString *)key;

/**
 * The key defining this kind of message
 * @discussion The key corresponding to this class. TO OVERRIDE.
 * @warning TO OVERRIDE.
 */
- (NSString *)key;

/**
 * Get a String representation
 * @return a string representation
 */
- (NSString*)toString;

NS_ASSUME_NONNULL_END
@end
