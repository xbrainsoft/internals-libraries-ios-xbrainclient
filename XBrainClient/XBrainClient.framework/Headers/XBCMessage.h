//
//  XBCMessage.h
//  XBrainClient
//
//  Created by Nicolas Mauri on 29/10/11.
//  Copyright (c) 2011 XBrain. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Enumeration defining all kinds of messages
 * @enum          kXBCMessageType
 * @discussion    Represents valid message types
 */
typedef NS_ENUM(NSInteger, kXBCMessageType) {
	kXBCTextMessage = 0,
	kXBCEventMessage = 1,
	kXBCCustomMessage = 2,
    kXBCUnknownMessage = 99
} ;

/**
 * Base class of all messages
 * @discussion    Represents an abstract message
 */
@interface XBCMessage : NSObject {
@protected
    NSMutableDictionary *_datas;
}

/**
 * Message's type
 */
@property (nonatomic, readonly) kXBCMessageType messageType;
/**
 * Message's timestamp
 */
@property (nonatomic, strong, nullable) NSDate* date;
/**
 * Message's data
 */
@property (nonatomic, weak, nullable) NSDictionary *messageDatas;

NS_ASSUME_NONNULL_BEGIN

/**
 * Create a message
 * @discussion    create a new message with the given data
 * @param data The message's data
 */
+ (nullable XBCMessage *)messageWithData:(NSData *)data;

/**
 * Create a message
 */
- (instancetype)init;

/**
 * Create a message
 * @discussion    initializes the message with a dictionary
 * @param dictionary a dictionary
 */
- (instancetype)initWithDictionary:(NSDictionary*)dictionary;

/**
 * Serialize to a JSON string
 * @discussion    serializes the message in JSON
 */
- (nullable NSString*)serializeAsJSONString;

/**
 * Get data associated to a given key
 * @discussion    gets a message data item corresponding to the given key
 * @param key The key
 */
- (nullable id)getMessageDataForKey:(NSString *)key;

/**
 * Set data associated to a given key
 * @discussion    sets a data for a specific key in our message data
 * @param data The data
 * @param key The key
 */
- (void)setMessageData:(id)data forKey:(NSString *)key;

/**
 * Send the message
 * @discussion    sends the message
 */
- (BOOL)send;

/**
 * Get a String representation of the message
 * @return a string representation
 */
- (NSString*)toString;

NS_ASSUME_NONNULL_END
@end
