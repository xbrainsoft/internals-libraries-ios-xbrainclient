//
//  ICNetworkState.h
//  XBrainClient
//
//  Created by Nicolas Mauri on 02/04/12.
//  Copyright (c) 2012 XBrain. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Enumeration defining the possible network states
 */
typedef NS_ENUM(NSInteger, ICNetworkStateEnum) {
    /**
     * Network is down
     */
    ICNetworkStateDown,
    /**
     * Network is up
     */
    ICNetworkStateUp
};

/**
 * Enumeration defining the available kinds of networks
 */
typedef NS_ENUM(NSInteger, ICNetworkTypeEnum) {
    /**
     * Cellular connectivity
     */
    ICNetworkTypeCellular,
    /**
     * WiFi connectivity
     */
    ICNetworkTypeWiFi,
    /**
     * Unknown connectivity
     */
    ICNetworkTypeUnknown
};

/**
 * Class whose purpose is to hold information related to network we expect to be connected to
 */
@interface ICNetworkInfo : NSObject
NS_ASSUME_NONNULL_BEGIN

/**
 * Connection state
 */
@property (nonatomic, readonly) ICNetworkStateEnum connectionState;
/**
 * Connection type
 */
@property (nonatomic, readonly) ICNetworkTypeEnum connectionType;
/**
 * Is host reachable
 */
@property (nonatomic, readonly) BOOL isHostReachable;

/**
 * Create an instance
 *
 *  @param state     a state
 *  @param type      a type
 *  @param reachable a boolean
 */
+ (ICNetworkInfo *)networkInfoWithState:(ICNetworkStateEnum)state type:(ICNetworkTypeEnum)type andHostReachability:(BOOL)reachable;

/**
 * Create an instance
 *
 *  @param state     a state
 */
+ (ICNetworkInfo *)networkInfoWithState:(ICNetworkStateEnum)state;

/**
 * Create an instance
 *
 *  @param state     a state
 *  @param type      a type
 *  @param reachable a boolean
 */
- (instancetype)initWithState:(ICNetworkStateEnum)state type:(ICNetworkTypeEnum)type andHostReachability:(BOOL)reachable;

/**
 * Get a String representation
 *
 *  @return a String representation
 */
- (NSString *)stringValue;

NS_ASSUME_NONNULL_END
@end
