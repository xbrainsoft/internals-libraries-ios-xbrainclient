//
//  NSCoder+JSON.h
//  XBrainClient
//
//  Created by Nicolas Mauri on 10/12/12.
//  Copyright (c) 2012 XBrain. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Category used to deserialize an array of custom message prototypes
 * @see           XBCCustomMessagePrototype
 */
@interface NSCoder (JSON)

/**
 * Decode an array identified by key into an array of custom message prototype (of class classObject)
 * @param classObject a class
 * @param key a key
 * @return an array
 */
- (nullable NSArray *)decodeArrayOfClass:(nonnull Class)classObject forKey:(nonnull NSString *)key;

/**
 * decodeObjectForKey
 *
 *  @param key a key
 *  @param classObject a class
 */
- (nullable id)decodeObjectForKey:(nonnull NSString *)key usingClass:(nullable Class)classObject;

@end
