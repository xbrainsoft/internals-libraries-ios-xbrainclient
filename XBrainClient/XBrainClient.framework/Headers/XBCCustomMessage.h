//
//  XBCCustomMessage.h
//  XBrainClient
//
//  Created by Nicolas Mauri on 29/10/11.
//  Copyright (c) 2011 XBrain. All rights reserved.
//

#import "XBCMessage.h"

/**
 * Represents a custom message
 * @discussion A custom message is represented by a key (its identifier) and a value (must be a valid JSON object)
 */
@interface XBCCustomMessage : XBCMessage

NS_ASSUME_NONNULL_BEGIN

/**
 * Creates and returns a custom message initialized with the given key and value
 * @param key a string corresponding to the object name server side
 * @param data a valid JSON object
 */
+ (XBCCustomMessage *)messageWithKey:(nullable NSString *)key andValue:(nullable NSDictionary *)data;

/**
 * Create an empty custom message
 */
- (instancetype)init;

/**
 * Create a custom message with the given NSDictionnary
 * @param dictionary a dictionary
 */
- (instancetype)initWithDictionary:(NSDictionary*)dictionary;

/**
 * Serialize its value as a JSON String
 * @return a string representing this message value in JSON
 */
- (NSString *)serializeValueAsJSONString;

/**
 * Get a String representation
 * @return a string representation
 */
- (NSString*)toString;

NS_ASSUME_NONNULL_END

@end
