//
//  XBCAgentConfig.h
//  XBrainClient
//
//  Created by Nicolas Mauri on 06/10/14.
//  Copyright (c) 2014 XBrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XBCConstants.h"

/**
 * Class whose purpose is to hold configuration information regarding agent connection settings 
 */
@interface XBCAgentConfig : NSObject {
}

@property (strong, nonatomic, nullable) NSString * scheme;
@property (strong, nonatomic, nullable) NSString * host;
@property (assign) int port;
@property (strong, nonatomic, nullable) NSString * uri;
@property (strong, nonatomic, nullable) NSString * instance;
@property (strong, nonatomic, nullable) NSString * agent;
@property (assign) BOOL anonymous;
@property (assign) BOOL enable;

NS_ASSUME_NONNULL_BEGIN
/**
 * Get a String representation
 *
 * @return a String representation
 */
- (NSString *) toString;

/**
 * Export as a dictionary
 *
 *  @return a dictionary
 */
- (NSDictionary *) serialize;

/**
 * Load from a dictionary
 *
 * @param dictionary a dictionary
 */
- (void) loadFromDictionary:(NSDictionary *) dictionary;

/**
 * Is equal to another <i>XBCAgentConfig</i> 
 *
 * @param config another <i>XBCAgentConfig</i>
 */
- (bool) equalsTo:(XBCAgentConfig *) config;
NS_ASSUME_NONNULL_END

@end
