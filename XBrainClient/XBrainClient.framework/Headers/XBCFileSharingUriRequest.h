//
//  XBCFileSharingUriRequest.h
//  XBrainClient
//
//  Created by xBrainsoft on 24/06/15.
//  Copyright (c) 2015 Nicolas Mauri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XBCCustomMessagePrototype.h"

/**
 * Class defining a message that is sent to request that the agent provide us the URI to use for "file sharing" features.
 */
@interface XBCFileSharingUriRequest : XBCCustomMessagePrototype

@end
