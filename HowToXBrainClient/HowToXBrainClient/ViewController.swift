//
//  ViewController.swift
//  HowToXBrainClient
//
//  Created by Christophe Cadix on 17/11/2015.
//  Copyright © 2015 xBrainSoft. All rights reserved.
//

import UIKit

/**
 Simple ViewController showcasing how to:
 * manage connection to an agent
 * send messages to it
 * receive messages from it
 
 - Important:
 Some mechanisms are based upon Objective-C's selector notion, and your Swift methods passed as selectors must be visible to
 the Obective-C runtime. By inheriting `UIViewController` this class also inherits `NSObject` and as a result its methods are
 visible to the Objective-C runtime. It is no longer the case for those made private but when marked `@objc` they still are.
 Note that a method marked `@IBAction` is implicitly marked `@objc` under the hood.
*/
class ViewController: UIViewController {
    
    @IBOutlet weak var labelVersion: UILabel!
    
    private lazy var XBrainClientVersion: String = "XBrainClient \(XBCClient.versionNumber()) (\(XBCClient.buildNumber()))"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSLog("configuration = \(AppConfig.configuration)")
        
        // We want to be notified when:
        // - the connection status changes
        // - and when the app enters the background, so that we can disconnect (and stop observing events)
        // - and when the app enters the foreground, so that we can restart observing events
        observeConnectionStatusChange()
        observeEnterBackground()
        observeEnterForeground()
        
        // Before connecting to the agent we need to configure the connection. The XBrainClient library does the rest.
        initXBrainClient()
        
        // Hook message reception
        registerMessageHandlers()
        
        // Set the initial state of our UI
        updateUI()
    }
    
    // ----------------------------------------------------------------------------------------------------------------------
    // #MARK: Agent
    // ----------------------------------------------------------------------------------------------------------------------
    
    /// *XBrainClient* configuration
    private func initXBrainClient() {
        if let agentConfig = AppConfig.agentConfig {
            XBCClient.configure(agentConfig)
        } else {
            NSLog("Was unable to configure XBCClient: no agent config found")
        }
    }
    
    /// Action attached to the *Connect/Disconnect* button
    @IBAction private func connect(sender: AnyObject) {
        NSLog("Connect/Disconnect button clicked")
        if XBCClient.isConnected() {
            XBCClient.disconnect()
        } else {
            XBCClient.connect()
        }
    }
    
    /// Computed property giving the connection state
    private var connectionState: kXBCConnectionState {
        return XBCClient.connectionState()
    }
    
    /// Callback for the *Send Text Message* button
    @IBAction private func sendTextMessage() {
        sendTextMessage("Hello, World !")
    }
    
    /// Callback for the *Send Custom Message* button
    @IBAction private func sendCustomMessage() {
        sendMessage(MyCustomMessage())
    }
    
    // ----------------------------------------------------------------------------------------------------------------------
    // #MARK: UI
    // ----------------------------------------------------------------------------------------------------------------------
    
    @IBOutlet weak private var connectionStateLabel: UILabel!
    @IBOutlet weak var toggleConnectButton: UIButton!
    @IBOutlet weak var logView: UITextView!
    
    /// Update the UI
    private func updateUI() {
        var stateLabel: String
        var buttonLabel = "Connect"
        var buttonEnabled = true
        
        switch connectionState {
            case .XBCConnecting:
                stateLabel = "Connecting..."
                buttonEnabled = false
            case .XBCDisconnected:
                stateLabel = "Disconnected"
            case .XBCConnected:
                stateLabel = "Connected"
                buttonLabel = "Disconnect"
            case .XBCConnectedAnonymously:
                stateLabel = "Connected (anonymous)"
                buttonLabel = "Disconnect"
        }
        
        connectionStateLabel.text = stateLabel
        toggleConnectButton.setTitle(buttonLabel, forState: UIControlState.Normal)
        toggleConnectButton.enabled = buttonEnabled
        
        labelVersion.text = XBrainClientVersion
    }
    
    private func scrollTextViewToBottom(textView: UITextView) {
        let length = textView.text.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)
        if length > 0 {
            let bottom = NSMakeRange(length - 1, 1)
            textView.scrollRangeToVisible(bottom)
        }
    }
    
    // ----------------------------------------------------------------------------------------------------------------------
    // #MARK: Notification management
    // ----------------------------------------------------------------------------------------------------------------------
    
    /// When app goes to the background we disconnect from the agent and ajust events we want to be watching
    func onApplicationDidEnterBackground(notification: NSNotification?) {
        XBCClient.disconnect()
        
        // now that we are in the background we no longer need to receive these notifications:
        observeConnectionStatusChange(false)
        observeEnterBackground(false)
    }
    
    /// When app comes back to the foreground we start watching events. Notice we said *back* because we start observing that
    /// event **after** the first time it happens
    func onApplicationWillEnterForeground(notification: NSNotification?) {
        NSLog("onApplicationWillEnterForeground")
        observeConnectionStatusChange()
        observeEnterBackground()
    }
    
    /// When the connection state changes we log it and update the UI
    func onConnectionStateChanged(notification: NSNotification?) {
        switch connectionState {
        case .XBCConnecting: NSLog("[connecting to agent...]")
        case .XBCDisconnected: NSLog("[disconnected from agent]")
        case .XBCConnected: NSLog("[connected to agent]")
        case .XBCConnectedAnonymously: NSLog("[anonymously connected to agent]")
        }
        updateUI()
    }
    
    private func observeConnectionStatusChange(enabled: Bool = true) {
        let name = kXBCConnectionStateChanged
        if (enabled) {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(onConnectionStateChanged(_:)),
                                                             name: name, object: nil);
        } else {
            NSNotificationCenter.defaultCenter().removeObserver(self, name: name, object: nil)
        }
    }
    
    private func observeEnterForeground(enabled: Bool = true) {
        let name = AppDelegate.kApplicationWillEnterForeground
        if (enabled) {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(onApplicationWillEnterForeground(_:)),
                                                             name: name, object: nil);
        } else {
            NSNotificationCenter.defaultCenter().removeObserver(self, name: name, object: nil)
        }
    }
    
    private func observeEnterBackground(enabled: Bool = true) {
        let name = AppDelegate.kApplicationDidEnterBackground
        if (enabled) {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(onApplicationDidEnterBackground(_:)),
                                                             name: name, object: nil);
        } else {
            NSNotificationCenter.defaultCenter().removeObserver(self, name: name, object: nil)
        }
    }
    
    // ----------------------------------------------------------------------------------------------------------------------
    // #MARK: Messages
    // ----------------------------------------------------------------------------------------------------------------------
    
    /// Set callbacks for messages reception
    private func registerMessageHandlers() {
        // Call onMessageReception whenever a XBCMessage is received
        XBCClient.registerMessageHandler(#selector(onMessageReception(_:)), target: self, forMessageClass: XBCTextMessage.self)
        XBCClient.registerMessageHandler(#selector(onMessageReception(_:)), target: self, forMessageClass: XBCCustomMessage.self)
    }
    
    /// Callback for message reception
    @objc private func onMessageReception(message: XBCMessage) {
        traceMessage(message, kind: .Received)
    }
    
    /// Send a text message to the agent
    private func sendTextMessage(string: String) {
        // This simple line of code is actually the simpler way to send a text message:
        // XBCClient.sendText(string)
        // Though here we instanciate a XBCTextMessage instead just to take advantage of the logging done by our sendMessage
        // method.
        sendMessage(XBCTextMessage(text: string))
    }
    
    /// Send a message and trace it in log/logView
    private func sendMessage(message: XBCMessage) {
        if message.send() {
            traceMessage(message, kind: .Sent)
        }
    }
    
    /// Trace sent/received message
    private func traceMessage(message: XBCMessage, kind: MessageKind) {
        let type: String
        let value: String
        
        switch message {
            case let message as XBCTextMessage:
                type = "Text"
                value = message.text ?? ""
            case is XBCCustomMessage:
                type = "Custom"
                value = message.toString()
            default:
                type = "Undefined"
                value = message.toString()
        }
        
        let logTrace = "[\(type)][\(kind)]: \(value)"
        
        let kindTrace: String
        switch kind {
            case .Received: kindTrace = "<<"
            case .Sent: kindTrace = ">>"
        }
        let logViewTrace = "\(kindTrace) [\(type)]: \(value) \n\n"
        
        NSLog(logTrace)
        dispatch_async(dispatch_get_main_queue()) {
            self.logView.text.appendContentsOf(logViewTrace)
            self.scrollTextViewToBottom(self.logView)
        }
    }
    
    private enum MessageKind {
        case Received, Sent
    }
}

