//
//  AppConfig.swift
//  HowToXBrainClient
//
//  Created by Christophe Cadix on 07/01/2016.
//  Copyright © 2016 xBrainSoft. All rights reserved.
//

/// Handles app configuration related stuff
struct AppConfig {
    
    /**
     Get the running configuration (`Release`, `Debug` or an empty string if there is none defined).
     - Warning: the value `$(CONFIGURATION)` for the key `Configuration` must be added in the Info.plist file.
    */
    static let configuration: String = {
        let t = NSBundle.mainBundle().infoDictionary?["Configuration"] as? String
        return t ?? ""
    }()
    
    private static let attributes: [String: AnyObject]? = {
        // read Configuration.plist file and pick what's defined for our current configuration
        guard let
            path = NSBundle.mainBundle().pathForResource("Configuration", ofType: "plist"),
            configurations = NSDictionary(contentsOfFile: path) else {
                return nil
        }
        return configurations[configuration] as? [String: AnyObject]
    }()
    
    /**
     Get an `XBCAgentConfig` object built upon configuration provided in `Configuration.plist` file, or `nil` if it was not
     possible to do so.
    */
    static let agentConfig: XBCAgentConfig? = {
        guard let
            attributes = attributes,
            serverAttributes = attributes["Server"] as? [String: AnyObject],
            scheme = serverAttributes["Scheme"] as? String,
            host = serverAttributes["Host"] as? String,
            port = serverAttributes["Port"] as? Int,
            uri = serverAttributes["Uri"] as? String,
            instance = serverAttributes["Instance"] as? String,
            agent = serverAttributes["Agent"] as? String,
            anonymous = serverAttributes["Anonymous"] as? Bool else {
                return nil
        }
        
        let agentConfig = XBCAgentConfig()
        agentConfig.scheme = scheme
        agentConfig.host = host
        agentConfig.port = Int32(port)
        agentConfig.uri = uri
        agentConfig.instance = instance
        agentConfig.agent = agent
        agentConfig.anonymous = anonymous
        return agentConfig;
    }()
}