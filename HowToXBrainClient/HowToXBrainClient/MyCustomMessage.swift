//
//  MyCustomMessage.swift
//  HowToXBrainClient
//
//  Created by Christophe Cadix on 04/01/2016.
//  Copyright © 2016 xBrainSoft. All rights reserved.
//

/// Simple custom message
class MyCustomMessage: XBCCustomMessagePrototype {
    
    var attr1: String = ""
    var attr2: Bool = false
    var attr3: Int = 0
    
    /// Provide the message String identifier
    override class func key() -> String {
        return "package.MyCustomMessage" // a unique identifier (usually defined server side)
    }
    
    override init() {
        super.init()
    }
    
    /// Deserialization (when message is received)
    required init!(coder aDecoder: NSCoder) {
        super.init()
        attr1 = aDecoder.decodeObjectForKey("Attr1") as! String
        attr2 = aDecoder.decodeBoolForKey("Attr2")
        attr3 = aDecoder.decodeIntegerForKey("Attr3")
    }
    
    /// Serialization (when message is about to be sent)
    override func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.attr1, forKey: "Attr1")
        aCoder.encodeBool(self.attr2, forKey: "Attr2")
        aCoder.encodeInteger(self.attr3, forKey: "Attr3")
    }
}