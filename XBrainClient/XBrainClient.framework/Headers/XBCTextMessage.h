//
//  XBCTextMessage.h
//  XBrainClient
//
//  Created by Nicolas Mauri on 29/10/11.
//  Copyright (c) 2011 XBrain. All rights reserved.
//

#import "XBCMessage.h"

/**
 * Represents a simple text message
 */
@interface XBCTextMessage : XBCMessage

NS_ASSUME_NONNULL_BEGIN

/**
 * Message's text content
 */
@property (nonatomic, readonly, nullable) NSString* text;

/**
 * Creates and returns a message initialized with the given text
 * @param text Message's text content
 */
+ (XBCTextMessage *)messageWithText:(NSString *)text;

/**
 * Create an empty message
 */
- (instancetype)init;

/**
 * Create a text message with the given NSDictionnary
 * @param dictionary a dictionary
 */
- (instancetype)initWithDictionary:(NSDictionary*)dictionary;

/**
 * Create a text message with the given text
 * @param text The text content
 */
- (instancetype)initWithText:(NSString*)text;

/**
 * Message's type as defined by the server
 */
@property (nonatomic, readonly, copy, nullable) NSString *type;

/**
 * Get a string representation
 */
- (NSString*)toString;

NS_ASSUME_NONNULL_END
@end
