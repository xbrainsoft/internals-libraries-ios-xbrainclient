//
//  XBCWebObject.h
//  XBrainClient
//
//  Created by Nicolas Mauri on 26/07/12.
//  Copyright (c) 2012 XBrain. All rights reserved.
//

#import "XBCCustomMessagePrototype.h"

/**
 * Class defining a message whose purpose is to describe a Webobject
 */
@interface XBCWebObject : XBCCustomMessagePrototype

@property (strong, nonatomic, nullable) NSString *identifier;

@property (strong, nonatomic, nullable) NSString *name;
@property (strong, nonatomic, nullable) NSString *version;
@property (strong, nonatomic, nullable) NSString *startupURI;
@property (strong, nonatomic, nullable) NSArray *dependencies;
@property (strong, nonatomic, nullable) XBCMessage *initializationData;

+ (nonnull instancetype)webObjectWithIdentifier:(null_unspecified NSString *)identifier;

/**
 * index file's full path
 */
@property (nonatomic, getter=getIndexFile, readonly, copy, nullable) NSString *indexFile;

/**
 * base URL
 */
@property (nonatomic, getter=getBaseURL, readonly, copy, nullable) NSURL *baseURL;

/**
 * loadSpecificationFile
 */
- (void)loadSpecificationFile;
@end
