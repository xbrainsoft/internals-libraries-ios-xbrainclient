//
//  XBCClient.h
//  XBrainClient
//
//  Created by Nicolas Mauri on 27/10/11.
//  Copyright (c) 2011 XBrain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XBCConstants.h"
#import "XBCMessage.h"
#import "ICNetworkInfo.h"
#import "XBCWebObjectHandler.h"
#import "XBCAgentConfig.h"

@class XBCMessageDispatcher;
@class XBCCustomMessagePrototype;
@class XBCClient;

// ------------------------------
//  IFSViewControllerDelegate
// ------------------------------
#pragma mark - IFSViewControllerDelegate

/**
 @brief Implement this protocol to present IFS View Controller. Only needed if you are using IFS to connect to the agent.
 */
@protocol XBCIFSViewControllerDelegate <NSObject>
- (void)presentIFSViewController:(null_unspecified UIViewController *)viewController;
@end

// ------------------------------
//  XBCClient
// ------------------------------
#pragma mark - XBCClient

/**
 * Main entry point of the framework. Used as a singleton.    
 */
@interface XBCClient : NSObject <NSURLConnectionDelegate> {
}

NS_ASSUME_NONNULL_BEGIN

/**
 * Shared instance
 */
+ (XBCClient *)sharedInstance;

#pragma mark -

/**
 * Returns the current version number
 */
+ (NSString*) versionNumber;

/**
 * Returns the current build number
 */
+ (NSString *)buildNumber;

+ (nullable NSString*)applicationDeviceId;/**< applicationDeviceId */

/**
 * View Controller delegate, used to present views
 * @param delegate a <i>XBCIFSViewControllerDelegate</i>
 */
+ (void)setViewControllerDelegate:(nullable id<XBCIFSViewControllerDelegate>)delegate;

#pragma mark configuration

/**
 * Sets agent configuration
 * @param config a <i>XBCAgentConfig</i>
 */
+ (void)configure:(XBCAgentConfig*)config;

#pragma mark connection

/**
 * Are we connected to the agent ?
 * @return YES if the connection has been successfully made, NO otherwise
 */
+ (BOOL)isConnected;

/**
 * Connect to the agent
 */
+ (void)connect;

/**
 * Connect to the agent using the given provider name and infos
 *
 *  @param providerName a provider name
 *  @param token        a token
 *  @param userId       a user identifier
 *  @param displayName  a display name
 *  @param firstName    a first name
 *  @param lastName     a last name
 *  @param email        an email
 *  @param birthDate    a birth date
 */
+ (void)connectWithProvider:(null_unspecified NSString *)providerName
                      token:(null_unspecified NSString *)token
                     userId:(null_unspecified NSString *)userId
                displayName:(null_unspecified NSString *)displayName
                  firstName:(null_unspecified NSString *)firstName
                   lastName:(null_unspecified NSString *)lastName
                      email:(null_unspecified NSString *)email
                  birthDate:(null_unspecified NSDate *)birthDate;

/**
 * Disconnect from the agent
 */
+ (void)disconnect;

/**
 * Update the provider token
 *
 *  @param token    a token
 *  @param provider a provider
 *  @param userId   a user identifier
 *  @param devideId a devide id
 */
+ (void)updateProviderToken:(null_unspecified NSString*)token
                      provider:(null_unspecified NSString*)provider
                     userId:(null_unspecified NSString*)userId
                   deviceId:(null_unspecified NSString*)devideId;

/**
 * Get the connection state
 * @return the current connection state
 */
+ (kXBCConnectionState)connectionState;

/**
 * Clear stored identity. A new identity will be created during next connection.
 */
+ (void)resetIdentity;

/**
 * Check if an identity exists for the current config
 */
+ (BOOL)hasIdentity;


#pragma mark messages

/**
 * Get the message dispatcher
 * @return the message dispatcher
 */
+ (XBCMessageDispatcher*) messageDispatcher;

/**
 * Send a text message
 * @param text the message's text
 */
+ (BOOL)sendText:(NSString *)text;

/**
 * Send a message
 * @param message the message to send
 */
+ (BOOL)sendMessage:(XBCMessage *)message;

/**
 * Dispatch a message
 * @param message the message to dispatch
 */
+ (void)dispatchMessage:(XBCMessage *)message;

/**
 * Register a message handler
 *
 *  @param selector     a selector
 *  @param target       a target
 *  @param messageClass a message class
 */
+ (void)registerMessageHandler:(null_unspecified SEL)selector
                        target:(null_unspecified id)target
               forMessageClass:(null_unspecified Class)messageClass;

/**
 * Unregister a message handler
 *
 *  @param target       a target
 *  @param messageClass a message class
 */
+ (void)unregisterMessageHandlerForTarget:(null_unspecified id)target
                          forMessageClass:(null_unspecified Class)messageClass;

/**
 * Register a prototype
 * @discussion use this method to register a class as a prototype. The class must inherit from <i>XBCCustomMessagePrototype</i>.
 * It allows the library to deserialize messages sent by the agent, as well as serializing them.
 * @param prototype a class
 * @see XBCCustomMessagePrototype
 */
+ (void)registerPrototype:(Class)prototype;

/**
 * Unregister a prototype
 * @param prototype a class
 * @see XBCCustomMessagePrototype
 */
+ (void)unregisterPrototype:(Class)prototype;

#pragma mark - WebObjects
/**
 * Add a new webobject handler
 * @param handler a <i>XBCWebObjectHandler</i>
 */
+ (void)registerWebObjectHandler:(id<XBCWebObjectHandler>)handler;

/**
 * Remove a webobject handler
 * @param handler a <i>XBCWebObjectHandler</i>
 */
+ (void)unregisterWebObjectHandler:(id<XBCWebObjectHandler>)handler;

/**
 * Clear webobject cache
 */
+ (void)clearWebObjectCache;

/**
 * Delete a webobject from the cache using its identifier
 * @param identifier webobject's identifier
 */
+ (void)deleteWebObjectFromCache:(NSString *)identifier;

#pragma mark tools

/**
 * Get the current network status
 * @warning Can freeze the main thread
 * @return the current network status. Warning, can freeze the main thread.
 */
+ (ICNetworkInfo*)networkInfo;

NS_ASSUME_NONNULL_END
@end
