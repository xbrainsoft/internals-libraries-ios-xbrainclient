//
//  XBrainClient.h
//  XBrainClient
//
//  Created by Nicolas Mauri on 24/09/14.
//  Copyright (c) 2014 XBrain. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for XBrainClient.
FOUNDATION_EXPORT double XBrainClientVersionNumber;

//! Project version string for XBrainClient.
FOUNDATION_EXPORT const unsigned char XBrainClientVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <XBrainClient/XBrainClient.h>

#import <XBrainClient/XBCConstants.h>
#import <XBrainClient/XBCClient.h>
#import <XBrainClient/XBCAgentConfig.h>
#import <XBrainClient/XBCMessage.h>
#import <XBrainClient/XBCCustomMessage.h>
#import <XBrainClient/XBCCustomMessagePrototype.h>
#import <XBrainClient/XBCTextMessage.h>
#import <XBrainClient/ICNetworkInfo.h>
#import <XBrainClient/XBCWebObject.h>
#import <XBrainClient/XBCWebObjectViewController.h>
#import <XBrainClient/XBCWebObjectHandler.h>
#import <XBrainClient/IFSViewController.h>
#import <XBrainClient/NSCoder+JSON.h>
#import <XBrainClient/JSONCoder.h>
