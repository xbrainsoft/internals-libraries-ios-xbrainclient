//
//  XBCConstants.h
//  XBrainClient
//
//  Created by Nicolas Mauri on 07/10/14.
//  Copyright (c) 2014 XBrain. All rights reserved.
//

#import <Foundation/Foundation.h>

// --------------------
//  XBCConnectionState
// --------------------
/**
 * Enumeration defining all possible connection states
 * @discussion    Defines valid connection states for an XBCClient instance
 */
typedef NS_ENUM(NSInteger, kXBCConnectionState) {
    kXBCConnectedAnonymously = 0,
    kXBCConnected = 1,
    kXBCConnecting = 2,
    kXBCDisconnected = 3
} ;

// --------------------
//  kXBCErrorCode
// --------------------
/**
 * Enumeration defining all possible connection-related errors
 * @discussion    Defines valid error codes
 */
typedef NS_ENUM(NSInteger, kXBCErrorCode) {
    kXBCIFSRegistrationCanceled,
    kXBCNotConnectedToInternet,
    kXBCRequestTimedOut,
    kXBCAgentUnreachable,
    kXBCUnknownError
} ;

// --------------------
//  Constants
// --------------------
/**
 * @discussion    XBCErrorDomain constant
 */
FOUNDATION_EXPORT NSString const* kXBCErrorDomain;

FOUNDATION_EXPORT NSString *const kXBCAgentConfigScheme;
FOUNDATION_EXPORT NSString *const kXBCAgentConfigHost;
FOUNDATION_EXPORT NSString *const kXBCAgentConfigPort;
FOUNDATION_EXPORT NSString *const kXBCAgentConfigUri;
FOUNDATION_EXPORT NSString *const kXBCAgentConfigInstance;
FOUNDATION_EXPORT NSString *const kXBCAgentConfigAgent;
FOUNDATION_EXPORT NSString *const kXBCAgentConfigAnonymous;
FOUNDATION_EXPORT NSString *const kXBCAgentConfigEnable;

/**
 * Notification key
 */
FOUNDATION_EXPORT NSString *const kXBCConnectionStateChanged;
